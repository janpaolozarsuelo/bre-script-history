# BRE Script History


### Installing

1. Clone the project repository

```
git clone https://janpaolozarsuelo@bitbucket.org/janpaolozarsuelo/bre-script-history.git
```

2. Import the project in IntelliJ
3. Update your local repository to get the latest from origin
4. Create /resources/local.properties

```
jsBreRootDir=[full path of BRE js files]
kotlinBreRootDir=[full path of BRE kt files]
rootDir=[full path of Android Project]
```
