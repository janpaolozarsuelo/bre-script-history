package util

import java.util.*

object Constant {

    private val properties by lazy {
        javaClass.getResourceAsStream("local.properties").let {
            Properties().apply { load(it) }
        }
    }

    val breRootDir = properties.let {
        properties.getProperty("breRootDir")
    }

    val jsBreRootDir = properties.let {
        properties.getProperty("jsBreRootDir")
    }

    val kotlinBreRootDir = properties.let {
        properties.getProperty("kotlinBreRootDir")
    }

    val rootDir = properties.let {
        properties.getProperty("rootDir")
    }

    val firstBranch = "R2"

    val secondBranch = "R3"
}