package util

import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

fun String.time()  =
        SimpleDateFormat("EEE MMM d kk:mm:ss yyyy ZZZZZ").parse(this).time

fun String.runCommand(workingDir: File?): BufferedReader? {
    val process = ProcessBuilder(*split(" ").toTypedArray())
            .directory(workingDir)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .start()

    if (!process.waitFor(30, TimeUnit.SECONDS)) {
        process.destroy()
        throw RuntimeException("execution timed out: $this")
    }
    if (process.exitValue() != 0) {
        return null
    }

    return if (process.inputStream != null) {
        BufferedReader(InputStreamReader(process.inputStream))
    } else {
        null
    }

}