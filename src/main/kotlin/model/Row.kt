package model

import util.time

data class Row(
        var js: String,
        var jsLastCommit: String?,
        var kt: String? = null,
        var ktLastCommit: String? = null
) {
    var isNeedToUpdate: Boolean = false
        get() {
            val jsTime = jsLastCommit?.time() ?: -1
            val kTTime = ktLastCommit?.time() ?: -1

            return jsTime >= kTTime
        }
}