import model.Row
import util.runCommand
import java.io.File
import java.io.PrintWriter
import java.lang.StringBuilder
import java.util.*

private const val flag = "Date:   "
private val map = mutableMapOf<String, Row>()

object Constant {

    private val properties by lazy {
        javaClass.getResourceAsStream("local.properties").let {
            Properties().apply { load(it) }
        }
    }

    val breRootDir = properties.let {
        properties.getProperty("breRootDir")
    }

    val jsBreRootDir = properties.let {
        properties.getProperty("jsBreRootDir")
    }

    val kotlinBreRootDir = properties.let {
        properties.getProperty("kotlinBreRootDir")
    }

    val rootDir = properties.let {
        properties.getProperty("rootDir")
    }

    val firstBranch = "R2"
    val secondBranch = "R3"
    val inputFileName = "input.txt"
}

object Input {

    private val resource by lazy { javaClass.getResource(Constant.inputFileName).readText() }

    val lines = resource.split("\n").filterNot { it.trim().startsWith("#") }
}

fun main(args: Array<String>) {

    moveToBranch(Constant.firstBranch)
    println("==============================================")

    runGitLogJs()

    println()
    println("==============================================")
    moveToBranch(Constant.secondBranch)
    println("==============================================")

    runGitLogKotlin()

    println()
    println("==============================================")

    generateCsv()
}

/**
 * Generate the CSV file for
 */
fun generateCsv() {

    val sb = StringBuilder()

    map.forEach { _, v ->
        if (v.ktLastCommit != null) {
            sb.append(v.js)
                    .append(',')
                    .append(v.jsLastCommit)
                    .append(',')
                    .append(v.kt)
                    .append(',')
                    .append(v.ktLastCommit)
                    .append(',')
                    .append(v.isNeedToUpdate)
                    .append('\n')
        }
    }

    val csvFileName = "output.csv"
    PrintWriter(File(csvFileName)).apply {
        write(sb.toString())
        close()
    }
    println("### CSV generated: $csvFileName ###")
}

fun runGitLogJs() {

    println("### JS BRE root dir: ${Constant.jsBreRootDir}")
    val fileBreRoot = File(Constant.jsBreRootDir)

    Input.lines.forEach {
        val trimmed = it.trim()
        val lastCommitDate = executeLog("git log -1 $trimmed", fileBreRoot)
        println("$trimmed,$lastCommitDate")

        map[trimmed] = Row(trimmed, lastCommitDate)
    }
}

fun runGitLogKotlin() {

    println("### Kotlin BRE root dir: ${Constant.kotlinBreRootDir}")
    val fileBreRoot = File(Constant.kotlinBreRootDir)
    Input.lines.forEach {
        val trimmed = it.trim()
                .replace(".", "_")
                .replace("_js", ".kt")
        val initialCommitDate = executeLog("git log --reverse $trimmed", fileBreRoot)

        println("$trimmed,$initialCommitDate")

        map[it.trim()]?.apply {
            kt = trimmed
            ktLastCommit = initialCommitDate
        }
    }
}

fun moveToBranch(branch: String) {
    println("Moving to $branch")
    val rootDir = File(Constant.rootDir)
    val output = executeCheckout("git checkout $branch", rootDir)
    println("Current branch: $output")
    Thread.sleep(5_000)
}

fun executeLog(command: String, workingDir: File?): String? {
    command.runCommand(workingDir)
            ?.readLines()
            ?.apply {
                for (line in this) {
                    if (line.contains(flag)) {
                        return line.replace(flag, "")
                    }
                }
            }
    return null
}

fun executeCheckout(command: String, workingDir: File?): String? {
    var log = ""
    command.runCommand(workingDir)
            ?.readLines()
            ?.apply {
                for (line in this) {
                    log = line
                }
            }
    return log
}